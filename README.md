# TelegramBasicBot

Lightweight Telegram API Wrapper for Python, easily expandable.


## Installation
### Step 1: Create an API Key
Create an API key by talking to @Botfather on Telegram ([Docs](https://core.telegram.org/bots))

### Step 2: Clone the repository
```
git clone https://gitlab.com/contendum/telegram/TelegramBasicBot.git
```

## Usage
### 1. Polling mode
```
from telegramBasicBot.telegramBasicBot import TelegramBot

myAPI_KEY = "Your API key"
myChatID = 123123

#Declare and initialize
tgBot = TelegramBot(myAPI_KEY)

#send a message
tgBot.sendText(myChatID, "Hello World!")

#read a update (returns a single message, photo, etc or None when there is no update)
print(tgBot.getNextUpdate())
```
### 2. Asynchronous mode
```
from telegramBasicBot.telegramBasicBot import TelegramBot

myAPI_KEY = "Your API key"
myChatID = 123123

class myTelegramBot(TelegramBot):
    def __init__():
        super.__init__(outputMode="event")
    #overwrite the update handlers for incuming updates you want to use

    #this function will be called from within the telegramBot thread whenever there is an update
    def _handleText(self, update):
        print(update)

tgBot = myTelegramBot(myAPI_KEY)

#send a message
tgBot.sendText(myChatID, "Hello World!")

```

## Support
Feel free to open an issue if you're having trouble.

## Contributing
Open to feature requests. Keep in mind that this bot is supposed to stay as lightweight as possible.

## License
GNU GPLv3

## Project status
Only bug fixes / maintenance for now

## Possible next features
- [ ] Send-Only-Mode (Disables polling for messages sent to the bot)
- [ ] Commandline-Mode
- [ ] Implement sendPhoto, sendVoice, sendVideo
