import requests
import json
import threading
import time
import logging

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

telegramPrefix = "https://api.telegram.org/bot"

class TelegramBot:
    """
    Asynchronous Telegram Wrapper:
    outputMode == "polling:
        - Call getNextUpdate for next update json
    outputMode = "event"
        - override handle functions to recieve messages
    
    
    Official API Documentation: https://core.telegram.org/bots/api
    """
    
    
    _longPollingTimeoutSeconds = 30 #Telegram supports long polling    
    _updateDelay = 1 #only between long polling cycles
    
    def __init__(self, token, outputMode="polling"):
        self._token = token
        self._lastUpdateID = 0
        self._stopUpdateThread = False
        self._updateList = []
        self._outputMode = outputMode
        
        #Setup and start update thread
        self.updateThread = threading.Thread(target = self._continousUpdate, daemon=True)
        self.updateThread.start()
        
    def _buildLink(self, queryString):
        return telegramPrefix + self._token + "/" + queryString
    
    def _getResultJson(self, query):
        return json.loads(requests.get(self._buildLink(query), self._longPollingTimeoutSeconds+1).content.decode())
    
    def _fillUpdateList(self):
        argString = "?offset=" + str(self._lastUpdateID + 1) + "&timeout=" + str(self._longPollingTimeoutSeconds)
        #request and store new updates
        results = self._getResultJson("getUpdates" + argString)
        if "result" not in results:
            return
        
        for result in results['result']:
            #make sure its a new update
            if self._lastUpdateID < result['update_id']:
                self._lastUpdateID = result['update_id']
                
            #Switch between output modes:
            if self._outputMode == "polling":
                #add to updateList
                self._updateList.append(result)


            if self._outputMode == "event":
                logger.debug(result)
                self._handleMessageTypes(result)
                        
                    
    def _handleMessageTypes(self, result):
        if "text" in result['message']:
            self._handleText(result)

        elif "voice" in result['message']:
            self._handleVoice(result)

        elif "photo" in result['message']:
            self._handlePhoto(result)
        
        elif "video" in result['message']:
            self._handleVideo(result)
                            
    def _handleText(self, update):
        logger.warning("Telegram: Text message recieved but no text handling defined")

    def _handleVoice(self, update):
        logger.warning("Telegram: Voice message recieved but no voice handling defined")
    
    def _handlePhoto(self, update):
        logger.warning("Telegram: Photo recieved but no photo handling defined")
        
    def _handleVideo(self, update):
        logger.warning("Telegram: Video recieved but no video handling defined")
        
    def _handleDocument(self, update):
        #Any file can be a document (videos, images, other file types)
        #Images can be sent as a document or image
        logger.warning("Telegram: Video recieved but no video handling defined")
        
    def sendText(self,chat, text, disableWebPagePreview = None, disableNotification = None):
        apiString = "sendMessage?chat_id=" + str(chat) + "&text=" + text
        
        if type(disableWebPagePreview) == bool:
            apiString += "&disable_web_page_preview=" + str(disableWebPagePreview)
        if type(disableNotification) == bool:
            apiString += "&disable_notification=" + str(disableNotification)
            
        result = self._getResultJson(apiString)
        
    def sendPhoto():
        #To be implemented
        pass
    
    def sendVoice():
        #To be implemented
        pass
    
    def sendVideo():
        #To be implemented
        pass
                            
    def _continousUpdate(self):
        while not self._stopUpdateThread:
            self._fillUpdateList()
            time.sleep(self._updateDelay)

    def getNextUpdate(self):
        if len(self._updateList) > 0:
            return self._updateList.pop(0)
        else:
            return None
